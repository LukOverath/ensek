using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ensek.data;
using ensek.initialization;
using ensek.meters;
using ensek.meters.validators;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace ensek
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ensek", Version = "v1" });
            });
            
            services.AddMemoryCache();
            services.AddSingleton<IDatabase, MemoryDatabase>();

            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IMeterDataRepository, MeterDataRepository>();

            services.AddTransient<IInitializer, AccountsInitializer>();

            services.AddTransient<IMeterDataImporter, MeterDataImporter>();
            services.AddTransient<IMeterDataValidator, MeterDataValidator>();

            services.AddTransient<IMeterDataItemValidator, MeterDataAccountIdValidator>();
            services.AddTransient<IMeterDataItemValidator, MeterDataReadValueValidator>();
            services.AddTransient<IMeterDataItemValidator, MeterDataItemExistsValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ensek v1"));
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
