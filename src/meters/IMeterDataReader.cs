namespace ensek.meters
{
    public interface IMeterDataReader
    {         
        void PrepareReading();

        bool Read();

        MeterData GetRecord();
    }
}