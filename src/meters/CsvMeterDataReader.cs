using System;
using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace ensek.meters
{
    public class CsvMeterDataReader : IMeterDataReader, IDisposable
    {
        private readonly CsvReader csvReader;
        private readonly bool readHeader;
        private readonly bool leaveOpen;
        private bool disposed = false;


        public CsvMeterDataReader(Stream csvStream, bool readHeader = true, bool leaveOpen = false)
        {
            _ = csvStream ?? throw new ArgumentNullException(nameof(csvStream));

            this.csvReader = new CsvReader(new StreamReader(csvStream), GetConfiguration());
            this.readHeader = readHeader;
            this.leaveOpen = leaveOpen;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public MeterData GetRecord()
        {
            return csvReader.GetRecord<MeterData>();
        }

        public void PrepareReading()
        {
            csvReader.Read();
            if (readHeader)
            {
                csvReader.ReadHeader();
            }
        }

        public bool Read()
        {
            return csvReader.Read();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                if (!leaveOpen)
                {
                    csvReader.Dispose();
                }
            }

            disposed = true;
        }

        private CsvConfiguration GetConfiguration()
        {
            return new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                ReadingExceptionOccurred = ReadError,
                DetectColumnCountChanges = true
            };
        }

        private bool ReadError(ReadingExceptionOccurredArgs args)
        {
            //log/handle the error
            //for now ignore and continue
            return false;
        }
    }
}