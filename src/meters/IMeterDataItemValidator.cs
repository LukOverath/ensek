namespace ensek.meters
{
    public interface IMeterDataItemValidator
    {
        bool Validate(MeterData data);         
    }
}