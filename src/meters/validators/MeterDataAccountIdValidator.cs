using ensek.data;

namespace ensek.meters.validators
{
    public class MeterDataAccountIdValidator : IMeterDataItemValidator
    {
        private readonly IAccountRepository accountRepository;

        public MeterDataAccountIdValidator(IAccountRepository accountRepository)
        {
            this.accountRepository = accountRepository;            
        }

        public bool Validate(MeterData data)
        {
            if (data?.AccountId == null)
            {
                return false;
            }

            return accountRepository.GetAccount(data.AccountId) != null;
        }
    }
}