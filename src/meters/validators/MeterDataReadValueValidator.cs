using System.Text.RegularExpressions;

namespace ensek.meters.validators
{
    public class MeterDataReadValueValidator : IMeterDataItemValidator
    {
        private const string regexPattern = "^[0-9]{5}$";

        public bool Validate(MeterData data)
        {
            return Regex.IsMatch(data?.MeterReadValue ?? string.Empty, regexPattern);
        }
    }
}