using ensek.data;

namespace ensek.meters.validators
{
    public class MeterDataItemExistsValidator : IMeterDataItemValidator
    {
        private readonly IMeterDataRepository meterDataRepository;

        public MeterDataItemExistsValidator(IMeterDataRepository meterDataRepository)
        {
            this.meterDataRepository = meterDataRepository;
        }

        public bool Validate(MeterData data)
        {
            if (data == null)
            {
                return false;
            }

            return !meterDataRepository.Exists(data);
        }
    }
}