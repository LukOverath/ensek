namespace ensek.meters
{
    public interface IMeterDataValidator
    {
        bool IsValid(MeterData data);
    }
}