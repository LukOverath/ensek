using System;
using System.Collections.Generic;
using System.Linq;

namespace ensek.meters
{
    public class MeterDataValidator : IMeterDataValidator
    {
        private IEnumerable<IMeterDataItemValidator> validators;

        public MeterDataValidator(IEnumerable<IMeterDataItemValidator> validators)
        {
            this.validators = validators ?? Enumerable.Empty<IMeterDataItemValidator>();
        }

        public bool IsValid(MeterData data)
        {
            foreach(var validator in validators)        
            {
                if (!validator.Validate(data))
                {
                    return false;
                }
            }

            return true;
        }
    }
}