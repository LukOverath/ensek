using System;
using ensek.data;

namespace ensek.meters
{
    public class MeterDataImporter : IMeterDataImporter
    {
        private readonly IMeterDataRepository meterDataRepository;
        private readonly IMeterDataValidator meterDataValidator;

        public MeterDataImporter(IMeterDataRepository meterDataRepository, IMeterDataValidator meterDataValidator)
        {
            this.meterDataRepository = meterDataRepository;
            this.meterDataValidator = meterDataValidator;
        }

        public ImportResult Import(IMeterDataReader reader)
        {
            _ = reader ?? throw new ArgumentNullException(nameof(reader));

            int recordsRead = 0;
            int recordsFailed = 0;

            reader.PrepareReading();
            while (reader.Read())
            {
                recordsRead++;
                if (!HandleRecord(reader.GetRecord()))
                {
                    recordsFailed++;
                }
            }

            return new ImportResult()
            {
                Imported = recordsRead - recordsFailed,
                Failed = recordsFailed
            };
        }

        private bool HandleRecord(MeterData meterData)
        {
            if (meterData == null)
            {
                return false;
            }

            return ValidateAndImport(meterData);
        }

        private bool ValidateAndImport(MeterData meterData)
        {
            if (!IsValid(meterData))
            {
                return false;
            }

            ImportRecord(meterData);
            return true;
        }

        private void ImportRecord(MeterData meterData)
        {
            meterDataRepository.StoreMeterData(meterData);
        }

        private bool IsValid(MeterData meterData)
        {
            return meterDataValidator.IsValid(meterData);            
        }
    }
}