namespace ensek.meters
{
    public interface IMeterDataImporter
    {
        ImportResult Import(IMeterDataReader reader);
    }
}