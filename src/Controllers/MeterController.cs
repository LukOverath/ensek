using ensek.meters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ensek.controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MeterController : ControllerBase
    {
        private readonly IMeterDataImporter meterDataImporter;

        public MeterController(IMeterDataImporter meterDataImporter)
        {
            this.meterDataImporter = meterDataImporter;            
        }

        [HttpPost("meter-reading-uploads")]        
        public ImportResult Post(IFormFile postedFile)
        {
            if (postedFile == null)
            {
                return null;                
            }            

            using (var dataReader = new CsvMeterDataReader(postedFile.OpenReadStream()))
            {
                return meterDataImporter.Import(dataReader);
            }
        }        
    }
}