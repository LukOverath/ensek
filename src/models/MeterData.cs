using System;

namespace ensek
{
    public class MeterData
    {
        public int AccountId { get; set; }

        public string MeterReadingDateTime { get; set; }

        public string MeterReadValue { get; set; }
    }
}