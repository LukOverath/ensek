namespace ensek
{
    public class ImportResult
    {
        public int Imported { get; set; }

        public int Failed { get; set; }
    }
}