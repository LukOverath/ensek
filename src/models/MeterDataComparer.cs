using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ensek
{
    public class MeterDataComparer : IEqualityComparer<MeterData>
    {
        public bool Equals(MeterData m1, MeterData m2)
        {
            if (object.ReferenceEquals(m1, m2))
                return true;

            if (m1 is null || m2 is null)
            {
                return false;
            }

            return m1.AccountId == m2.AccountId &&
                    m1.MeterReadingDateTime == m2.MeterReadingDateTime &&
                    m1.MeterReadValue == m2.MeterReadValue;
        }

        public int GetHashCode([DisallowNull] MeterData obj)
        {
            return HashCode.Combine(obj.AccountId, obj.MeterReadingDateTime, obj.MeterReadValue);
        }
    }
}