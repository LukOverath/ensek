using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using ensek.data;

namespace ensek.initialization
{
    public class AccountsInitializer : IInitializer
    {
        private const string accountData = "initialization/Test_Accounts.csv";
        private readonly IAccountRepository accountRepository;

        public AccountsInitializer(IAccountRepository accountRepository)
        {
            this.accountRepository = accountRepository;
        }

        public void Initialize()
        {
            using (var fStream = File.OpenRead(accountData))
            {
                using (var dataReader = new StreamReader(fStream))
                {
                    using (var reader = new CsvReader(dataReader, System.Globalization.CultureInfo.InvariantCulture))
                    {
                        ImportAccounts(reader);
                    }
                }
            }
        }

        private void ImportAccounts(CsvReader reader)
        {
            var accounts = new List<Account>();

            reader.Read();
            reader.ReadHeader();

            while (reader.Read())
            {
                accounts.Add(reader.GetRecord<Account>());
            }

            accountRepository.StoreAccounts(accounts);
        }
    }
}