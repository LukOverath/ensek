using System.Threading.Tasks;

namespace ensek.initialization
{
    public interface IInitializer
    {
        void Initialize();
    }
}