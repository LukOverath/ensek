using System;
using System.Collections.Generic;
using System.Linq;

namespace ensek.data
{
    public class MeterDataRepository : IMeterDataRepository
    {
        private const string tableName = "meterdata";
        private readonly IDatabase database;

        public MeterDataRepository(IDatabase database)
        {
            this.database = database;
        }

        public bool Exists(MeterData data)
        {
            return database.RetrieveRecords<MeterData>(tableName)?.Contains(data, new MeterDataComparer()) ?? false;
        }

        public bool StoreMeterData(MeterData data)
        {
            if (!Exists(data))
            {
                StoreData(data);
                return true;
            }

            return false;
        }

        public IEnumerable<MeterData> GetMeterData()
        {
            return database.RetrieveRecords<MeterData>(tableName) ?? Enumerable.Empty<MeterData>();
        }

        private void StoreData(MeterData data)
        {
            database.StoreRecord(tableName, data);
        }

    }
}