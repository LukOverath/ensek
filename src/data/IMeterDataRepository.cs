using System.Collections.Generic;
using ensek.meters;

namespace ensek.data
{
    public interface IMeterDataRepository
    {
        IEnumerable<MeterData> GetMeterData();

        bool StoreMeterData(MeterData data);

        bool Exists(MeterData data);
    }
}