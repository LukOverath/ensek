using System.Collections.Generic;
using System.Linq;

namespace ensek.data
{
    public class AccountRepository : IAccountRepository
    {
        private readonly string tableName = "accounts";

        private readonly IDatabase database;

        public AccountRepository(IDatabase database)
        {
            this.database = database;            
        }
        
        public Account GetAccount(int accountId)
        {
            var accounts = database.RetrieveRecords<Account>(tableName); 
            return accounts?.Where(a => a.AccountId == accountId).FirstOrDefault();
        }

        public void StoreAccounts(IEnumerable<Account> accounts)
        {
            this.database.StoreRecords(tableName, accounts);
        }
    }
}