using System.Collections.Generic;

namespace ensek.data
{
    public interface IAccountRepository
    {
        void StoreAccounts(IEnumerable<Account> accounts);

        Account GetAccount(int accountId);
    }
}