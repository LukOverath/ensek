using System;
using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;

namespace ensek.data
{
    public class MemoryDatabase : IDatabase
    {
        private readonly IMemoryCache memoryCache;

        public MemoryDatabase(IMemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }

        public IEnumerable<T> RetrieveRecords<T>(string tableName)
        {
            return this.memoryCache.Get<IList<T>>(tableName);
        }

        public void StoreRecords<T>(string tableName, IEnumerable<T> records)
        {
            var data = new List<T>(records);
            this.memoryCache.Set(tableName, data);
        }

        public void StoreRecord<T>(string tableName, T record)
        {
            var items = this.memoryCache.GetOrCreate<IList<T>>(tableName, entry => { return new List<T>(); });
            items.Add(record);
            StoreRecords(tableName, items);
        }
    }
}