using System.Collections.Generic;

namespace ensek.data
{
    public interface IDatabase
    {
        IEnumerable<T> RetrieveRecords<T>(string tableName);

        void StoreRecords<T>(string tableName, IEnumerable<T> records);

        void StoreRecord<T>(string tableName, T record);
    }
}