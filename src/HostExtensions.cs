using ensek.initialization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ensek
{
    public static class HostExtensions
    {
        public static IHost Initialize(this IHost host)
        { 
            var services = host.Services.GetServices<IInitializer>();
            foreach (var initializer in services)
            {
                initializer.Initialize();                
            }

            return host;
        }
    }
}