using System;
using ensek;
using ensek.data;
using ensek.meters.validators;
using Moq;
using Xunit;

namespace test.ensek
{
    public class ValidationTests
    {
        [Theory]
        [InlineData("12345", true)]
        [InlineData("56789", true)]
        [InlineData("11111", true)]
        [InlineData("00000", true)]
        [InlineData("55555", true)]
        [InlineData("1111A", false)]
        [InlineData("123456", false)]
        [InlineData("AAAAA", false)]
        [InlineData("-----", false)]
        [InlineData("", false)]
        public void GivenMeterReadingData_WhenReadingValidated_ExpectedResultIsCorrect(string value, bool expectedResult)
        {
            var data = new MeterData()
            {
                AccountId = 1,
                MeterReadValue = value
            };

            var validator = new MeterDataReadValueValidator();
            Assert.Equal(expectedResult, validator.Validate(data));
        }

        [Theory]
        [InlineData(123, true)]
        [InlineData(456, false)]
        public void GivenMeterReadingData_WhenAccountValidated_ExpectedResultIsCorrect(int givenAccountId, bool expectedResult)
        {
            var accountId = 123;
            var data = new MeterData()
            {
                AccountId = givenAccountId,
                MeterReadValue = "12345"
            };
            var account = new Account()
            {
                AccountId = accountId
            };

            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.GetAccount(accountId)).Returns(account);


            var validator = new MeterDataAccountIdValidator(repoMock.Object);
            Assert.Equal(expectedResult, validator.Validate(data));
        }
    }
}
